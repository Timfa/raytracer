window.onload = function()
{
	console.log("Loading Everything");
	GameWindow.init(0, 0, window.innerWidth - 2, window.innerHeight - 2);
	GameWindow.fullscreen = true;

	GameInput.g_init();

	Canvas.canvas.requestPointerLock = Canvas.canvas.requestPointerLock ||
			     element.mozRequestPointerLock ||
			     element.webkitRequestPointerLock;
	// Ask the browser to lock the pointer
	
	Canvas.canvas.onclick = function() {
		Canvas.canvas.requestPointerLock();
	  }

	World.loadLevel(0);

	setInterval(function()
	{
		var stepTime = Date.now();
		var stepTimeCurr = (stepTime - stepPrevious) * 0.001;
		
		if(isNaN(stepTimeCurr))
			stepTimeCurr = 0;

		GameInput.g_updateKeys();
		Player.update(stepTimeCurr);

		

		this.stepPrevious = stepTime;
	}, 1);

	GameWindow.redraw();
}

var stepPrevious = 0;