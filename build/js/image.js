function image(url, subImgTotal, perRow)
{
	subImgTotal = subImgTotal || 1;
	perRow = perRow || 1;

    this.subImages = subImgTotal;
    this.subImagesPerRow = perRow;
	
	var body = document.body;
    body.insertAdjacentHTML('beforeend', '<img id="image" src="' + url + '" style="display:none;"></img>');
    this.g_image = document.getElementById('image');
    document.getElementById('image').id = "loadedImage";

	Object.defineProperty(this, "width",
	{
		get: function()
		{
			return this.g_image.width / this.subImagesPerRow;
		}
	});

	Object.defineProperty(this, "height",
	{
		get: function()
		{
			return this.g_image.height / (Math.ceil(this.subImages / this.subImagesPerRow) + 0);
		}
	});

	this.draw = function(sx, sy, sw, sh, dx, dy, dw, dh, rotation, alpha)
	{	
		var shouldDraw = true;
		
		if(dx < -Math.sqrt(Math.pow(dw, 2) + Math.pow(dh, 2)))
			shouldDraw = false;
			
		if(dy < -Math.sqrt(Math.pow(dw, 2) + Math.pow(dh, 2)))
			shouldDraw = false;
		
		if(dx > GameWindow.width + Math.sqrt(Math.pow(dw, 2) + Math.pow(dh, 2)))
			shouldDraw = false;
		
		if(dy > GameWindow.height + Math.sqrt(Math.pow(dw, 2) + Math.pow(dh, 2)))
			shouldDraw = false;

		if(shouldDraw)
		{
			var context = GameWindow.canvasContext;
			context.save(); 
			var oldAlpha = context.globalAlpha;
			context.globalAlpha = alpha;

			context.translate(dx, dy);
			context.rotate(rotation * (Math.PI/180));

			context.drawImage(this.g_image, sx, sy, sw, sh, -dw/2, -dh/2, dw, dh);

			context.restore(); 
			context.globalAlpha = oldAlpha;
		}
	}
}