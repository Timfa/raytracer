var Player = 
{
    x: 0,
    y: 0,
    rotation: 0,
    pitch: 0,
    horizontalResolution: 200,
    gunX:0.6,
    gunY:0.6,
    currentGun:0,
    guns:3,

    machineGunImg: null,
    shotgunImg: null,
    pulseRifleImg: null,

    init: function()
    {
        this.machineGunImg = new image("images/machinegun.png");
        this.shotgunImg = new image("images/shotgun.png");
        this.pulseRifleImg = new image("images/pulserifle.png");
    },

    update: function(deltaTime)
    {
        if(GameInput.isHeld(37) || GameInput.isHeld(65)) // left arrow
        {
            var rot = this.rotation - Math.PI/2;
            if(rot < 0)
                rot += Math.PI*2;
            if(rot > Math.PI*2)
                rot -= Math.PI*2

            var forward = Vector.fromAngle(rot);

            if(World.rayCast(this.x, this.y, rot).distance > 5)
            {
                this.x += forward.x * deltaTime * 60;
                this.y += forward.y * deltaTime * 60;
            }
        }

        if(GameInput.isHeld(39) || GameInput.isHeld(68)) // right arrow
        {
            var rot = this.rotation + Math.PI/2;
            if(rot < 0)
                rot += Math.PI*2;
            if(rot > Math.PI*2)
                rot -= Math.PI*2

            var forward = Vector.fromAngle(rot);

            if(World.rayCast(this.x, this.y, rot).distance > 5)
            {
                this.x += forward.x * deltaTime * 60;
                this.y += forward.y * deltaTime * 60;
            }
        }

        //this.pitch +=  //((GameInput.mousePosition.y - (GameWindow.height / 2)) / (GameWindow.height/2)) * Math.PI;
        
        if(this.rotation > Math.PI * 2)
            this.rotation -= Math.PI * 2;

        if(this.rotation < 0)
            this.rotation += Math.PI * 2;

        if(GameInput.isHeld(38) || GameInput.isHeld(87)) // up arrow
        {
            var forward = Vector.fromAngle(this.rotation);

            if(World.rayCast(this.x, this.y, this.rotation).distance > 5)
            {
                this.x += forward.x * deltaTime * 60;
                this.y += forward.y * deltaTime * 60;
            }
        }

        if(GameInput.isHeld(40) || GameInput.isHeld(83)) // down arrow
        {
            var forward = Vector.fromAngle(this.rotation);

            var rot = this.rotation + Math.PI;
            if(rot < 0)
                rot += Math.PI*2;
            if(rot > Math.PI*2)
                rot -= Math.PI*2

            if(World.rayCast(this.x, this.y, rot).distance > 5)
            {
                this.x -= forward.x * deltaTime * 40;
                this.y -= forward.y * deltaTime * 40;
            }
        }

        if(GameInput.isHeld(188)) // < arrow
        {
            this.horizontalResolution-= this.horizontalResolution * 0.1 * deltaTime;
            if(this.horizontalResolution < 30)
                this.horizontalResolution = 30;
        }
        if(GameInput.isHeld(190)) // > arrow
        {
            this.horizontalResolution+= this.horizontalResolution * 0.1 * deltaTime;
            if(this.horizontalResolution > GameWindow.width)
                this.horizontalResolution = GameWindow.width;
        }
    },

    draw: function()
    {
        var forward = Vector.fromAngle(this.rotation).stretch(25);

        Canvas.drawRect(this.x - 10, this.y - 10, 20, 20, 0xff0000);
        Canvas.drawLine(this.x, this.y, this.x + forward.x, this.y + forward.y, 0xff00ff);
    },

    draw3D: function()
    {
        var rad = Math.PI / (this.horizontalResolution*2);

        for(var i = 0; i < this.horizontalResolution; i++)
        {
            var offsetP = -0.5 + (i / this.horizontalResolution);
            var offset = offsetP * this.horizontalResolution;

            var rotationalOffset = offset * rad;

            var rot = this.rotation + rotationalOffset;

            if(rot < 0)
                rot += Math.PI * 2;
            if(rot > Math.PI * 2)
                rot -= Math.PI * 2;

            var rayHit = World.rayCast(this.x, this.y, rot);

            var color = {
                R: rayHit.tile.Color.R,
                G: rayHit.tile.Color.G,
                B: rayHit.tile.Color.B
            }

            var light = 1;

            switch(rayHit.dir)
            {
                case "north":
                    break;
                case "east":
                    color.R *= 0.9;
                    color.G *= 0.9;
                    color.B *= 0.9;
                    light = 0.9;
                    break;
                case "west":
                    color.R *= 0.8;
                    color.G *= 0.8;
                    color.B *= 0.8;
                    light = 0.8;
                    break;
                case "south":
                    color.R *= 0.7;
                    color.G *= 0.7;
                    color.B *= 0.7;
                    light = 0.7;
                    break;
            }

            var deltaAngle = this.rotation - rot;
            rayHit.distance *= Math.cos(deltaAngle);

            var xLine = i * (GameWindow.width / this.horizontalResolution);
            var widthLine = (GameWindow.width / this.horizontalResolution);

            var height = (World.size * GameWindow.height) / rayHit.distance;

            var yLine = (GameWindow.height / 2 - height / 2) + (-this.pitch * (World.size * GameWindow.height - height) * 0.005);
            var imgyLine = (GameWindow.height / 2 - height / 2) + (-this.pitch * (World.size * GameWindow.height - height) * 0.005);

            if(rayHit.tile.Image)
            {
                var subImg = Math.min(rayHit.tile.Image.g_image.width - widthLine, Math.max(1, rayHit.tileX * rayHit.tile.Image.g_image.width));
                rayHit.tile.Image.draw(subImg, 0, widthLine, rayHit.tile.Image.height, xLine, imgyLine + height/2, widthLine, height, 0, light);
            }
            else
            {
                Canvas.drawRect(xLine, yLine, widthLine, height, "rgb(" + color.R + "," + color.G + "," + color.B + ")");
            }
        }

        var gunImg = null;
        switch(this.currentGun)
        {
            case 0:
                gunImg = this.shotgunImg;
                break;
            case 1:
                gunImg = this.machineGunImg;
                break;
            case 2:
                gunImg = this.pulseRifleImg;
                break;
        }

        var p = (GameWindow.height / 1080)
        var w = p * gunImg.g_image.width;
        var h = p * gunImg.g_image.height;

        Canvas.drawImage(gunImg.g_image, GameWindow.width * this.gunX, GameWindow.height * this.gunY, w, h);
    }
}

Player.init();