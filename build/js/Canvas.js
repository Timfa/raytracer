// Wrapper for some canvas drawing functions

var Canvas = 
{
    get canvas()
    {
        return GameWindow.canvas;
    },

    get context()
    {
        GameWindow.canvasContext.webkitImageSmoothingEnabled = false;
        GameWindow.canvasContext.mozImageSmoothingEnabled = false;
        GameWindow.canvasContext.imageSmoothingEnabled = false;
        return GameWindow.canvasContext;
    },

    drawText: function(text, x, y, size, font, color)
    {
        this.context.font = font? size + 'px ' + font : size + 'px Arial';
        
        this.context.fillStyle = color;
        this.context.strokeStyle = color;
        this.context.textAlign = "center";
        this.context.textBaseline="middle";

        this.context.fillText(text, x, y);
    },

    drawLine(xStart, yStart, xEnd, yEnd, color)
    {
        this.context.fillStyle = color;
        this.context.strokeStyle = color;
        
        this.context.beginPath();
        this.context.moveTo(xStart, yStart);
        this.context.lineTo(xEnd, yEnd);
        
        this.context.stroke();
    },

    drawRect(xStart, yStart, xEnd, yEnd, color)
    {
        this.context.fillStyle = color;
        this.context.strokeStyle = color;
        
        this.context.beginPath();
        this.context.fillRect(xStart, yStart, xEnd, yEnd);
    },

    drawImage(img, x, y, w, h)
    {
        this.context.drawImage(img, x, y, w, h);
    }
}