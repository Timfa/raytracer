var GameWindow = 
{
	width: 0,
	
	height: 0,

	g_width: 0,
	g_height: 0,

	fullscreen: false,
	
	backgroundColor: 
	{
		r: 0,
		g: 0,
		b: 0	
	},
	
	windowElement: null,
	elementID: "GameBuilder_Window",
	
	init: function(x, y, width, height)
	{
		GameWindow.x = x;
		GameWindow.y = y;

		GameWindow.width = width;
		GameWindow.height = height;

		GameWindow.g_width = width;
		GameWindow.g_height = height;
		
		var body = document.body;
		body.insertAdjacentHTML('beforeend', '<canvas id="' + GameWindow.elementID + '" style="position: absolute; left: ' + x + 'px; top: ' + y + 'px; border: 1px solid black;" width="' + GameWindow.width + '" height="' + GameWindow.height + '"></canvas>');
		
		//$("body").append('<div id="' + GameWindow.elementID + '" style="position: absolute; left: ' + x + 'px; top: ' + y + 'px; width: ' + GameWindow.width + 'px; height: ' + GameWindow.height + 'px; overflow: hidden; border: 1px solid black;"></div>');
		GameWindow.windowElement = document.getElementById(GameWindow.elementID);
		
		GameWindow.canvas = document.getElementById(GameWindow.elementID);
		
		GameWindow.canvasContext = GameWindow.canvas.getContext('2d');

		console.log("GAME: Window initialized: [" + width + ", " + height + "]");
	},
	
	redraw: function()
	{
		if(GameWindow.fullscreen)
		{
			GameWindow.width = window.innerWidth - 2;
			GameWindow.height = window.innerHeight - 2;
		}
		else
		{
			GameWindow.width = GameWindow.g_width;
			GameWindow.height = GameWindow.g_height;
		}

		GameWindow.canvas.width = GameWindow.width;
		GameWindow.canvas.height = GameWindow.height;

		GameWindow.canvasContext.fillStyle = "rgb(" + GameWindow.backgroundColor.r + "," + GameWindow.backgroundColor.g + "," + GameWindow.backgroundColor.b + ")"; // sets the color to fill in the rectangle with
        GameWindow.canvasContext.fillRect(0, 0, GameWindow.width, GameWindow.height);
        
        Player.draw3D();

		requestAnimationFrame(GameWindow.redraw);
	}
}