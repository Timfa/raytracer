var Tiles = 
{
    Nothing:
    {
        ID: 0,
        Color: {
            R: 255,
            G: 255,
            B: 255
        },
        Solid: false
    },
    Stone:
    {
        ID: 1,
        Color: {
            R: 155,
            G: 155,
            B: 155
        },
        Image: new image("images/stone.png", 16, 16),
        Solid: true
    },
    Finish:
    {
        ID: 2,
        Color: {
            R: 255,
            G: 255,
            B: 0
        },
        Solid: false
    },
    Wood:
    {
        ID: 3,
        Color: {
            R: 168,
            G: 102,
            B: 50
        },
        Image: new image("images/wood.png", 16, 16),
        Solid: true
    },
    findByID(id)
    {
        for (const [key, value] of Object.entries(Tiles)) 
        {
            if(value.ID == id)
                return value;
        }
    }
}